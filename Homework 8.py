import numpy as np
import matplotlib.pyplot as plt
import scipy.ndimage
import pylab as pl

Image = open("m33.dat", "r")
b = np.fromfile(Image, dtype=np.uint16)
bimage = b.reshape((1000,1000))

#normal plot
"""
plt.imshow(bimage)
plt.savefig('orig.jpg')
"""
#changed color map
"""
imgplot = plt.imshow(bimage)
imgplot.set_cmap('spectral')
plt.savefig('new.jpg')
"""
#contour 
"""
plt.contour(bimage)
plt.savefig('contour.jpg')
"""
#Finding the brightest spot and (x,y) Location
def bright_spot( img ):
    bright_pixel = img.argmax()
    ymax,xmax = np.unravel_index(bright_pixel,img.shape)
    return xmax,ymax

x,y = bright_spot(bimage)
print "the brights spot is at: ","x= ", x,"y= ",y
print " the value at this spot is: ", bimage[y,x]

#Defining the glactic center as a box around pixel 530,530 and about 60 on each side
#Finds Average Brightness at Galactic Center
print "the average value in center of galaxy is:  ",(np.sum(bimage[500:560,500:560]))/3600

plt.imshow(bimage)
plt.show()

