import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import pylab as p

#Constants that will be used
h = 6.626e-34
c = 3.0e+8
k = 1.38e-23
wavelengths = np.arange(1e-9, 3e-6, 1e-9)

def plankfunc(wavelength, T, r, d):
    a = 2.0*h*c**2
    d /= 3.09e-16
    wavelength /= 1e9
    b = h*c/(wavelength*k*T)
    intensity = a/((wavelength**5) * (np.exp(b) - 1.0)) #Finds intensity based on given parameters
    Intensity = intensity*(r**2)/(d**2) #multiply by (r/d)^2
    flux = Intensity*4*np.pi #Removes str units
    return flux

plotrange = range(100,1500,5)
barrange = range(100,1500,50)

p.plot(plotrange,[plankfunc(wave,5777.,6.96e9,1000)/(h*c/wave)*(p.pi*1.2**2 * .8 * .3 * 1000) for wave in plotrange], color='y', label='Sun')
p.xlabel(r'$\lambda$ (cm)')
p.ylabel(r'$\phi(\lambda) (T)$')
p.legend(loc=1)
p.title("$\phi$ vs. $\lambda$")
p.bar(barrange,[plankfunc(wave,5777.,6.96e9,1000)/(h*c/wave)*(p.pi*1.2**2 * .8 * .3 * 1000) for wave in barrange], color='y', label='Sun')
p.show()
