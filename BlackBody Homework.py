import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

#Constants that will be used
h = 6.626e-34
c = 3.0e+8
k = 1.38e-23
wavelengths = np.arange(1e-9, 3e-6, 1e-9)

def plankfunc(wave, T, r, d):
    a = 2.0*h*c**2
    b = h*c/(wave*k*T)
    print a 
    print b
    intensity = a/((wave**5) * (np.exp(b) - 1.0)) #Finds intensity based on given parameters
    Intensity = intensity*(r**2)/(d**2) #multiply by (r/d)^2
    flux = Intensity*4*np.pi #Removes str units
    print intensity
    print Intensity
    print flux
    return flux
    
intensity = plankfunc(wavelengths, 5778, 700000000, 10)
#plot a histogram of intensity

hist, bins = np.histogram(intensity , bins = 20)
print np.sum(bins)
plt.bar(wavelengths*1.0e9, intensity)
plt.show()