import matplotlib.pyplot as plt
import numpy as np
import matplotlib.mlab as mlab
import math

TotalHumans = 107602708000.
WorldPop = 7295925000.
USAPop = 32040500.

def NearVal(array, value): #For a given value, finds the value in the array closest to it
    NVal = (np.abs(array - value)).argmin()
    return array[NVal]

#Useful parameters, 100 IQ, STD 15, etc.
Mean = 100.
Sig = 15
IQ = np.linspace(0,300,600)
y = mlab.normpdf(IQ,Mean,Sig)
plt.ylabel('IQ Distribution')
plt.xlabel('IQ')

#Look at the top values of IQ
topIQ = np.linspace(100,300,600)
#Following Functions/equations solve how many people out of sample have IQ above stated amount
T= mlab.normpdf(topIQ,Mean,Sig)*USAPop
S = mlab.normpdf(topIQ,Mean,Sig)*TotalHumans
FindTe = np.where(T == (NearVal(T,0.12)))
FindSe = np.where(S == (NearVal(S,0.12)))

FindTgr = np.where(T<= (NearVal(T , 0.12)))
FindSgr = np.where(S<= (NearVal(S , 0.12)))

print np.sum(T[FindTgr]), " people in the United States with IQ of at least " , topIQ[FindTe]
print np.sum(S[FindSgr]), " people who ever lived with IQ of at least ", topIQ[FindSe]
plt.plot(IQ,y)
plt.show()

#For IQ of 200
print "Number of people with IQ of 200 alive today ", mlab.normpdf(200,Mean,Sig)*WorldPop

