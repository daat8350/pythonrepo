import numpy as np
import math as mt
import matplotlib.pyplot as plt

#Gets wavelength and flux from the text file, turns into Workable Data
w, flux = np.genfromtxt('poissspec.txt', skip_header = 1).T

#Planck's Constant, Speed of light, Boltzmann Constant
h = 6.626e-34
c = 2.998e8
k = 1.38e-23

"""plt.plot(w, flux)
plt.show()"""
#Function to Normalize Future Functions
norm = lambda x: ((x - x.min())/(x.max()-x.min()))

y = norm(flux)
#BlackBody Spectrum to Apply ChiSquared Statisitcs to
BBF= lambda w,T : (2.*c/(w**4*(np.exp(h*c/(w*k*T))-1)))
#Free-Free Bremsstrahlung Spectrum to Apply ChiSquared Statistics to
fff= lambda w,T : np.exp(-h*c/(w*k*T))/w
#Normalized BlackBody Spectrum
nbb = lambda w,T : norm(BBF(w,T))
#Normalized FFB Spectrum
nff = lambda w,T : norm(fff(w,T))
"""plt.plot(w,y)
plt.show()"""
#Function to apply the ChiSquared Statistic
chisquared = lambda obs, exp : ((obs - exp)**2/exp).sum()
#Temperature scale to test
Temp = 10**np.linspace(5.,8.,1000)
#Empty Array to be filled with Temperature Values
chisqBB=np.empty(Temp.shape)
chisqFF=np.empty(Temp.shape)

#Fills the Array with values of the BBS and FFBS applied to the ChiSquared
for i,T in enumerate(Temp):
    chisqBB[i] = chisquared(nbb(w*1e-10, T)+1,y +1)
    chisqFF[i] = chisquared(nff(w*1e-10, T)+1,y +1)
    
#Plots the Data on same graph to be compared
plt.plot(Temp,chisqBB)
plt.plot(Temp,chisqFF)
plt.ylabel('Chi Squared')
plt.xlabel('Temp (Log)')
plt.title('S Statistic Plot')
plt.show()

chisqBBmin = chisqBB.min()
chisqFFmin = chisqFF.min()
print chisqBBmin
print chisqFFmin
BBTempmin = 6.65e6
FFTempmin = 3.75e7
print (chisqBBmin,BBTempmin)
print (chisqFFmin,FFTempmin)

#Fit Both for Intensity, Temperature
