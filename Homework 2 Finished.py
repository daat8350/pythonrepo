import numpy as np
import matplotlib.pyplot as plt

x = [1.,2.,3.,4.]
y = [1.,7.,3.2,9.7]
dy = [0.1,0.2,0.15,0.3]

plt.plot(x,y,'d')
plt.errorbar(x,y,dy,xerr=None)
plt.axis([0,5,0,10])
plt.show()
