#!/C:\Users\Dan\PythonProjects
from numpy import *
import csv
import matplotlib.pyplot as plt


x,y,z,qx,qy,qz = loadtxt('rays.txt', unpack = True)

avg_x = sum(x)/len(x)
avg_y = sum(y)/len(y)

X = x - avg_x
Y = y - avg_y

plt.scatter(X,Y, color = 'blue', marker ='.')

xN = x[:25]
yN = y[:25]
zN = z[:25]
qxN = qx[:25]
qyN = qy[:25]
qzN = qz[:25]

av_xN = sum(xN)/len(xN)
av_yN = sum(yN)/len(yN)
XN = xN -av_xN
YN = yN -av_yN

with open('rays.txt', 'w') as F:
    for F1, F2, F3, F4, F5, F6 in zip(xN,yN,zN,qxN,qyN,qzN):
        print >> F, F1, F2, F3, F4, F5, F6 

plt.scatter(XN,YN)

plt.show()
