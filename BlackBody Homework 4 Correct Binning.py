import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

#Constants that will be used
h = 6.626e-34
c = 3.0e+8
k = 1.38e-23
wavelengths = np.arange(1e-9, 3e-6, 1e-9)

def plankfunc(wave, T, r, d):
    a = 2.0*h*c**2
    b = h*c/(wave*k*T)
    intensity = a/((wave**5) * (np.exp(b) - 1.0)) #Finds intensity based on given parameters
    Intensity = intensity*(r**2)/(d**2) #multiply by (r/d)^2
    flux = Intensity*4*np.pi #Removes str units
    return flux
    
intensity = plankfunc(wavelengths, 5778, 700000000, 10)
#plot a histogram of intensity

lambdabins = np.arange(1e-9, 3e-6, 1.5e-7) #twenty bins of data
bar_width = .2
for bin in lambdabins:
	print plankfunc(bin,5778,700000000,10)
plt.bar(plankfunc(lambdabins,5778,700000000,10),lambdabins,bar_width)
print plankfunc(lambdabins,5778,700000000,10).sum(), "total intensity of the Sun at 10 pc"

plt.show()