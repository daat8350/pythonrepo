import numpy as np
import pylab as pl
import math as math
import os
import time
import matplotlib.pyplot as plt
import astropy as ast
from astropy.convolution import convolve

import sys

class Target:
    def _init_(self,name='Unnamed',nx=100,ny=100,asperpix=0.1):
        self.name = name
        self.mp = np.float32(np.zeros((nx,ny)))
        self.asperpix = asperpix
        self.xc = nx/2
        self.yc = ny/2
        self.dr = math.pi/180
        
        
    def add_star(self, xp=0. , yp=0., lum=1.):
        x = self.xc + xp/self.asperpix
        y = self.yc + yp/self.asperpix
        self.mp[x,y] = lum
    
    def add_planet(self.xp=0.,yp=0. , inc=0. , az=0. , rad=1., phase=90. , lum=1.):
        xtt=rad*np.cos(phase*self.dr)
        ytt=rad*np.sin(phase*self.dr)
        xt=xtt*np.cos(az*self.dr)+ytt*np.sin(az*self.dr)+xp
        yt=ytt*np.cos(az*self.dr)-xtt*np.sin(az*self.dr)+yp
        z= self.xc +xt/self.asperpix
        y= self.yc = yt/self.asperpix
        
        