import numpy as np
import matplotlib.pyplot as plt
from scipy.misc import imread
import Image
<<<<<<< HEAD
import math as math
=======
>>>>>>> aeae7cfaf92683c8b5a8ede86f358b39ce6836f4
import pdb

#Constants
BHConst = 2.95e3 #2*G*c**(-2) is approximately 2.95km/SolMass
Pars = 3.08567758e16 #Parsecs to Meters

#Get original Image
def image(ImageName):
    im = Image.open(ImageName)
<<<<<<< HEAD
    row, col = im.size
    pixels = im.load()
    data = np.zeros((row*col,5))
=======
    row, col = im.size 
    data = np.zeros((row*col,5))
    pixels = im.load()

>>>>>>> aeae7cfaf92683c8b5a8ede86f358b39ce6836f4
    for i in range(row):
        for j in range(col):
            r,g,b = pixels[i,j]
            data[i*col+j,:]=r,g,b,i,j
<<<<<<< HEAD
    return data, pixels
#Here we take elements of the Im array into x, y coordinates to be used in the transformation into polar later

Im, pixels = image('Linux.jpg')
x = Im[:,3]
y = Im[:,4]

xlen = np.unique(x).size
ylen = np.unique(y).size

xcent = xlen/2
ycent = ylen/2
M = 300 #This is the real size of the background image in Parsecs. Used to find scale of each pixel.This value is specifically the radius of the MWG
PixelSize = (2*M*Pars)/xlen #Sets scale of the pixel
B = 500
print x
print y
print xlen
print ylen
print xcent
print ycent

def Scale(Mass,N,B): #This scales all distance parameters as pixels
    Rs = Mass*BHConst/PixelSize #Mass in SolMass, gives Rs in meters
    Dd = N*Rs/PixelSize #N is number of Rs away from Blackhole, gives distance in meters
    Dds =  Pars*B/PixelSize #Gives distance in meters from Image to BH , should be Ds>>Dd>>Rs
    Ds =  Dd+Dds/PixelSize
    D = (Dd*Dds/Ds)/PixelSize #Effective Lens Distance
    scale = np.asarray([Rs, Dd, Dds, Ds,D]) 
    return scale

def LensEquation(Mass, N, M, x, y, centerx, centery):#Function to transform x,y values into new x,y prime values baised on Gravitational Lensing
    LensParam = Scale(Mass, N, M)#REMINDER: [0] = Rs, [1] = Dd, [2] = Dds, [3] = Ds, [4] = D 
    NewX = np.empty(xlen*ylen)
    NewY = np.empty(xlen*ylen)
    
    for i in x:# this loop take each x,y pixel, transforms into polar. Acts on R, returns Rnew.
        for j in y:
            xR = (i - centerx)
            yR= (j - centery)
            R = np.sqrt(xR**2 + yR**2)
            
            if xR == 0:
                theta = np.pi/2
            else:
                theta = math.atan(yR/xR)
                
            if R <= LensParam[4]:#Only Lenses pixels within the critical lensing distance
                alpha = 2*LensParam[0]*R**-1
                Rnew = R + alpha * LensParam[3]
            elif R<=1:#Sets up Einstein Ring
                alpha = 2*LensParam[0]
                Rnew = LensParam[4] + alpha*LensParam[3]/LensParam[4]
                
                
            else :# Distances outside Ring should not be lensed
                Rnew = R 
                
            xnew = (Rnew * math.cos(theta)) + i
            #if xnew > xlen: So this loop and the ynew counterpart should get rid of values outside the original range of the image, because there is no intensity associated with those pixels. 
                #I searched google and didn't find anything useful on how to do this. Given a little more time I'm sure I could figure something out
            NewX[i*ylen+j]= xnew
            
            ynew = (Rnew * math.sin(theta)) + j 
            #if ynew>ylen:
                
            NewY[i*ylen+j] = ynew
                    
    return NewX, NewY
    
xnew, ynew = LensEquation(10, 20, B, x, y, xcent, ycent)

inds = ynew.argsort()
DataNew = Im.copy()
DataNew[:,-2:] = np.vstack((xnew.round(), ynew.round())).T
DataNew = DataNew[inds,:]
pixels[DataNew[:,-2],DataNew[:,-1]] = Im[:,:3].T #Error here, It says integer required, but it should be working with the arrays
plt.imshow(pixels)
plt.show()
pdb.set_trace()



#Things still to do:
# 1. Create If Statement that deals with einstein ring (Should be done)
# 2. Create a catch that gets rid of value outside the range of x,y in for loop (Not Done)

#Reasons this code is not finished. 
#It does not want to replace x,y values in pixels with those generated
#It is giving me error: Needs Integer
# I have not figured out how to deal with the values outside my original image

#I want to continue this project and refine it after this class. 
#I will probably approach the problem in a different way, either with raytracing or using the gravitational potential, or both. 
#My current method seems very limited compared to these other methods               

                  

            
            
            
            
      
            
            
            
    
    
    

    
    
            




=======

    pdb.set_trace()
    return data


# SO I'M A LITTLE CONFUSED ON WHERE YOUR ISSUE IS. THE CODE ABOVE MAKES TOTAL
# SENSE AND WORKS WELL. YOUR METHOD IS THAT YOU HAVE TO FIND THE DISTANCE EACH
# PIXEL IS FROM THE LENS, AND THEN DO DIFFERENT THINGS DEPENDING ON WHETHER OR
# NOT THAT DISTANCE SATISFIES CERTAIN CONDITIONS. YOU CAN DEAL WITH THOSE 
# CONDITIONS USING IF STATEMENTS, AND YOU CAN FIND THE DISTANCES USING THE 
# DISTANCE EQUATION.

# FOR EXAMPLE, IF MY LENS IS LOCATED AT (100,100), AND I USE THE SCALING OF
# THE IMAGE (PHYSICAL UNITS TO PIXEL UNITS) TO SAY THAT THE LENS HAS A 
# DIAMETER OF A 100 PIXELS, THEN I WOULD DO THE FOLLOWING:
#    dist = ((Im[:,3:]-np.array([100,100]).reshape((1,2)))**2).sum(1)
#    np.where(dist < 100/2., inside, outside)
#
# INSIDE WILL CONTAIN ALL THE PIXELS INSIDE THE LENS, OUTSIDE THE REST
#
# YOU COULD THEN OPERATE ON THE PIXELS USING THESE ARRAYS AND TRANSFORMING THEM
# AND MATCHING THOSE TO THEIR RGB VALUES FROM THEIR PREVIOUS POSITIONS


Im = image('terra_ir.jpg')
print Im
pdb.set_trace()


Rs = lambda Mass: Mass*BHConst #Mass in SolMass, gives Rs in meters
Dd = lambda Mass, N: N*Rs #N is number of Rs away from Blackhole, gives distance in meters
Dds = lambda M: Pars*M #Gives distance in meters from Image to BH , should be Ds>>Dd>>Rs
Ds = lambda Dd, Dds: Dd+Dds


def AngleofDeflection(Mass,ImageData):
    Rs = Mass*BHConst
    def thetafunc(ImageData):
        for i in range(row):
            for j in range(col):
                pass
    #alpha = (2*Rs)/(
    return
    
def warp(Image):
    
    
    return warpedIm

print Im[500]
>>>>>>> aeae7cfaf92683c8b5a8ede86f358b39ce6836f4






