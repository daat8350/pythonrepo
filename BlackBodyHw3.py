# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm

#Define Constants: Planck's Constant h, Speed of light c, Stephan-Boltzman k
h = 6.626e-34
c = 3.0e+8
k = 1.38e-23
#Set scale for accepted wavelengths of photons and interval(step)
lambdas= np.arange(1e-9,3e-6,1e-9)

#Blackbody plot as a function of tempurature, radius, and distance to Earth

def plotbb(temp,rad,dist):
    
        def fluxplot(wave,T,r,d): #Planck Function
            a = 2.0*h*c**2
            b = h*c/(wave*k*T)
            intensity = a/((wave**5)*(np.exp(b)-1.0)) #Calculates Intesity
            SInt=intensity*(r**2)/(d**2) #Finds Intensity at Earth
            flux=SInt*4*np.pi #Corrected Units
            return flux
        def SCTemp(temp):
            temp = temp-500.
            Temp = temp/30000.
            tempcolor = cm.get_cmap("gist_rainbow")
            starcolor = tempcolor(Temp)
            return starcolor
        flux = fluxplot(lambdas,temp,rad,dist)
        plt.plot(lambdas*1e+9,flux, c = SCTemp(temp))
        plt.grid(True)
        plt.title('Flux per Wavelength')
        plt.ylabel('Watts per SR per meter cubed')
        plt.xlabel('Wavelength in nm')
    #Putting it all together and showing the plot
plt.show(plotbb(5600,696000000.,149597870700)) #This is for the Sun at 1 AU. Put in different parameters to get a different plot